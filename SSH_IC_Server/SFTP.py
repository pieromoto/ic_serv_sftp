import pysftp
from paramiko import *
import os

'''
    Use to Upload file
'''
class ServerConnector:

    def __init__(self, host, usr, pwd):
        self.host = host
        self.usr = usr
        self.pwd = pwd

    def startConnection(self):
        try:
            self.conn = pysftp.Connection(host=self.host,username=self.usr,password=self.pwd)
            print("Connected to server")
            return True
        except pysftp.ConnectionException:
            print("Connection problems: Maybe Server is down or your connection is not working properly")
            return False
        except pysftp.AuthenticationException:
            print("Authentication Problems: Maybe your username or password is wrong")
            return False

    def handle_command(self, str):
        command = input(str)
        if(command == "close"):
            self.closeConnection()
            exit()
        elif(command == "list"):
            self.listDir()
        elif(command == "upload"):
            filePath = input("Enter file path: ")
            self.uploadFile(filePath)
            print("Upload done")
        elif(command == "rsp_upload"):
            filePath = input("Enter file path: ")
            remotePath = input("Enter remote path: ")
            self.uploadFileSpecificPath(filePath,remotePath)
            print("Upload done")
        elif(command == "upload_d_server"):
            filePath = input("Enter file path: ")
            port = input("Enter open port: ")
            remoteFolder = input("Enter remote stored folder: ")
            path = os.path.dirname(filePath)
            fileName = os.path.basename(filePath)
            self.uploadDjangoFolder(path,fileName,remoteFolder,port)
            print("Upload done")
        elif(command == "help" or command == "h"):
            print('''
            list - show all file and folder in remote server\n
            upload - upload file to default remote path\n
            rsp_upload - upload file to specific remote path\n
            upload_d_server - upload django server to remote path\n
            cd <path> - go to remote path\n
            current - show current directory\n
            close - close connection''')
        elif(command[0:2] == "cd"):
            path = command[3:]
            self.conn.chdir(path)
        elif(command == "current"):
            print(self.conn.pwd)
        else:
            print("No command found.")

    def listDir(self):
        data = self.conn.listdir()
        for lst in data:
            print(lst)

    def closeConnection(self):
        self.conn.close()

    def uploadFile(self, filePath):
        self.conn.put(filePath)

    def uploadFileSpecificPath(self, filePath, RemotePath):
        self.conn.put(filePath,RemotePath)

    def uploadDjangoFolder(self, Path, fileName, remoteFolder,openPort):
        if(self.conn.exists(remoteFolder)):
            self.conn.rmdir(remoteFolder)
        self.conn.mkdir(remoteFolder)
        self.conn.put(Path+"/"+fileName, remoteFolder)
        self.conn.close()

        # #open ssh connection
        # sshConn = SSHClient()
        # sshConn.connect(hostname=self.host,username=self.usr,password=self.pwd)
        # #unzip file
        # sshConn.exec_command("unzip " + RemoteServerFolder + "/" + fileName)
        # #run server
        # sshConn.exec_command("cd " + RemoteServerFolder)
        # sshConn.exec_command("cd " + fileName[:-4])
        # sshConn.exec_command("screen -r")
        # sshConn.exec_command("python manage.py runserver 161.246.94.246:" + openPort)
        # #close
        # sshConn.close()
        # self.startConnection()
