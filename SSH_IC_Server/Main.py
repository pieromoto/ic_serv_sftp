from SFTP import ServerConnector


def main():
    host = input("Enter Hostname: ")
    usr = input("Enter Username: ")
    pwd = input("Enter Password: ")

    #create connection
    connection = ServerConnector(host, usr, pwd)
    if(connection.startConnection()):
        while(1):
            connection.handle_command("Enter command: ");
    else:
        exit()

main()